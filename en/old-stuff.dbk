<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE appendix PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
  <!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<appendix id="ap-old-stuff" lang="en">
<title>Managing your &oldreleasename; system before the upgrade</title>
<para>
This appendix contains information on how to make sure you can install or
upgrade &oldreleasename; packages before you upgrade to &releasename;.  This should only be
necessary in specific situations.
</para>
<section id="old-upgrade">
<title>Upgrading your &oldreleasename; system</title>
<para>
Basically this is no different from any other upgrade of &oldreleasename; you've been
doing.  The only difference is that you first need to make sure your package
list still contains references to &oldreleasename; as explained in <xref
linkend="old-sources"/>.
</para>
<para>
If you upgrade your system using a Debian mirror, it will automatically be
upgraded to the latest &oldreleasename; point release.
</para>
</section>

<section id="old-sources">
<title>Checking your APT source-list files</title>
<para>
  If any of the lines in your APT source-list files (see <ulink
  url="https://manpages.debian.org/&releasename;/apt/sources.list.5.html">sources.list(5)</ulink>)
  contain references to <quote><literal>stable</literal></quote>, this is effectively pointing to
  &releasename; already. This might not be what you want if you are not yet ready
  for the upgrade.  If you have already run <command>apt update</command>,
  you can still get back without problems by following the procedure below.
</para>
<para>
If you have also already installed packages from &releasename;, there probably
is not much point in installing packages from &oldreleasename; anymore.  In
that case you will have to decide for yourself whether you want to continue or
not.  It is possible to downgrade packages, but that is not covered here.
</para>
<para>
  As root, open the relevant APT source-list file (such as
  <filename>/etc/apt/sources.list</filename>) with your favorite
  editor, and check all lines beginning with
  <literal>deb http:</literal>, <literal>deb https:</literal>,
  <literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>,
  <literal>URIs: http:</literal>, <literal>URIs: https:</literal>,
  <literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal>
  for a reference to <quote><literal>stable</literal></quote>.  If you find
  any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>.
</para>
<para>
  If you have any lines starting with <literal>deb file:</literal> or
  <literal>URIs: file:</literal>, you will have
  to check for yourself if the location they refer to contains a
  &oldreleasename; or &releasename; archive.
</para>
<important>
  <para>
    Do not change any lines that begin with <literal>deb cdrom:</literal> or
    <literal>URIs: cdrom:</literal>.
    Doing so would invalidate the line and you would have to
    run <command>apt-cdrom</command> again.  Do not be alarmed if a
    <literal>cdrom:</literal> source line refers to <quote><literal>unstable</literal></quote>.
    Although confusing, this is normal.
  </para>
</important>
<para>
If you've made any changes, save the file and execute
</para>
<screen>
# apt update
</screen>
<para>
to refresh the package list.
</para>
</section>

<section id="old-config">
<title>Removing obsolete configuration files</title>
<para>
Before upgrading your system to &releasename;, it is recommended to remove old
configuration files (such as <filename>*.dpkg-{new,old}</filename> files under
<filename>/etc</filename>) from the system.
</para>
</section>

<section id="switch-utf8">

<title>Upgrade legacy locales to UTF-8</title>
<para>
  Using a legacy non-UTF-8 locale has been unsupported by desktops and
  other mainstream software projects for a long time. Such locales
  should be upgraded by running <command>dpkg-reconfigure
  locales</command> and selecting a UTF-8 default. You should also
  ensure that users are not overriding the default to use a legacy
  locale in their environment.
</para>

</section>

</appendix>

