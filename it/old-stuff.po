# Italian translation of Debian release notes
# Copyright (C) 2015 by Software in the Public Interest
# This file is distributed under the same license as the Debian Squeeze release notes package.
# Luca Brivio <lucab83@infinito.it>, 2007
# Luca Monducci <luca.mo@tiscali.it>, 2009, 2010, 2011
# Vincenzo Campanella <vinz65@gmail.com>, 2009.
# Beatrice Torracca <beatricet@libero.it>, 2013, 2015, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian Jessie (8.0) release notes - old-stuff.po\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 16:18+0200\n"
"PO-Revision-Date: 2019-04-02 17:25+0200\n"
"Last-Translator: Beatrice Torracca <beatricet@libero.it>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "it"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Gestire il proprio sistema &oldreleasename; prima dell'avanzamento"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Questa appendice contiene informazioni su come accertarsi di poter "
"aggiornare o installare i pacchetti di &oldreleasename; prima di aggiornare "
"a &releasename;. Questo dovrebbe essere necessario solo in casi particolari."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Aggiornare il proprio sistema &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"In linea di principio non vi è alcuna differenza rispetto a qualsiasi altro "
"aggiornamento effettuato in precedenza per &oldreleasename;. L'unica "
"differenza è che dapprima sarà necessario accertarsi che il proprio elenco "
"dei pacchetti contenga ancora i riferimenti a &oldreleasename; come "
"illustrato in <xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Se si aggiorna il proprio sistema utilizzando un mirror Debian, esso sarà "
"aggiornato automaticamente all'ultimo point release (rilascio minore) di "
"&oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
#| msgid "Checking your sources list"
msgid "Checking your APT source-list files"
msgstr "Controllare i propri file source-list per APT"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
#| msgid ""
#| "If any of the lines in your <filename>/etc/apt/sources.list</filename> "
#| "refer to <quote><literal>stable</literal></quote>, it effectively points "
#| "to &releasename; already. This might not be what you want if you are not "
#| "ready yet for the upgrade.  If you have already run <command>apt update</"
#| "command>, you can still get back without problems by following the "
#| "procedure below."
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"https://"
"manpages.debian.org/&releasename;/apt/sources.list.5.html\">sources.list(5)</"
"ulink>)  contain references to <quote><literal>stable</literal></quote>, "
"this is effectively pointing to &releasename; already. This might not be "
"what you want if you are not yet ready for the upgrade.  If you have already "
"run <command>apt update</command>, you can still get back without problems "
"by following the procedure below."
msgstr ""
"Se qualsiasi riga nei propri file source-list di APT (vedere <ulink url="
"\"https://manpages.debian.org/&releasename;/apt/sources.list.5.html"
"\">sources.list(5)</ulink>) contiene riferimenti a <quote><literal>stable</"
"literal></quote>, in effetti sta già puntando a &releasename;. Ciò potrebbe "
"non essere quello che si vuole se non si è ancora pronti per l'avanzamento. "
"Se si è già eseguito <command>apt update</command>, si può ancora tornare "
"indietro senza problemi seguendo la procedura illustrata in seguito."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Se sono già stati installati pacchetti anche da &releasename;, probabilmente "
"non ha più molto senso installare pacchetti da &oldreleasename;. In questo "
"caso si dovrà decidere se si desidera continuare o meno. È possibile il "
"<quote>downgrade</quote> dei pacchetti, ma non è un argomento trattato qui."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
#| msgid ""
#| "Open the file <filename>/etc/apt/sources.list</filename> with your "
#| "favorite editor (as <literal>root</literal>) and check all lines "
#| "beginning with <literal>deb http:</literal>, <literal>deb https:</"
#| "literal>, <literal>deb tor+http:</literal>, <literal>deb tor+https:</"
#| "literal> or <literal>deb ftp:</literal> for a reference to "
#| "<quote><literal>stable</literal></quote>.  If you find any, change "
#| "<literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Da root, aprire il file sources-list di APT (come ad esempio <filename>/etc/"
"apt/sources.list</filename>) con il proprio editor preferito e si esaminino "
"tutte le righe che cominciano con <literal>deb http:</literal>, <literal>deb "
"https:</literal>, <literal>deb tor+http:</literal>, <literal>deb tor+https:</"
"literal>, <literal>URIs: http:</literal>, <literal>URIs:https</literal>, "
"<literal>URIs: tor+http:</literal> o <literal>URIs: tor+https:</literal>, "
"cercando un riferimento a <quote><literal>stable</literal></quote>. Se ve "
"n'è qualcuno, si cambi <literal>stable</literal> in <literal>&oldreleasename;"
"</literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
#| msgid ""
#| "If you have any lines starting with <literal>deb file:</literal>, you "
#| "will have to check for yourself if the location they refer to contains an "
#| "&oldreleasename; or a &releasename; archive."
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Se vi sono righe che cominciano con <literal>deb file:</literal> o "
"<literal>URIs: file:</literal>, si deve controllare da sé se gli indirizzi "
"cui si riferiscono contengono un archivio di &oldreleasename; o di "
"&releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
#| msgid ""
#| "Do not change any lines that begin with <literal>deb cdrom:</literal>.  "
#| "Doing so would invalidate the line and you would have to run <command>apt-"
#| "cdrom</command> again.  Do not be alarmed if a <literal>cdrom:</literal> "
#| "source line refers to <quote><literal>unstable</literal></quote>.  "
#| "Although confusing, this is normal."
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Non si modifichi alcuna riga che inizia con <literal>deb cdrom:</literal> o "
"<literal>URIs: cdrom:</literal>, in quanto in tal caso si invaliderebbe la "
"riga e si dovrebbe eseguire nuovamente <command>apt-cdrom</command>. Non ci "
"si allarmi se una fonte <literal>cdrom:</literal> fa riferimento a "
"<quote><literal>unstable</literal></quote>: sebbene sia motivo di "
"confusione, questo è normale."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "Se si sono fatte delle modifiche, si salvi il file e si esegua"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, no-wrap
msgid "# apt update\n"
msgstr "# apt update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "per aggiornare la lista dei pacchetti."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr "Rimuovere file di configurazione obsoleti"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Prima di aggiornare il proprio sistema a &releasename;, è raccomandata la "
"rimozione dei vecchi file di configurazione (come i file <filename>*.dpkg-"
"{new,old}</filename> in <filename>/etc</filename>) dal sistema."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:94
msgid "Upgrade legacy locales to UTF-8"
msgstr "Passare dai locale obsoleti a UTF-8"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:96
msgid ""
"Using a legacy non-UTF-8 locale has been unsupported by desktops and other "
"mainstream software projects for a long time. Such locales should be "
"upgraded by running <command>dpkg-reconfigure locales</command> and "
"selecting a UTF-8 default. You should also ensure that users are not "
"overriding the default to use a legacy locale in their environment."
msgstr ""
"L'uso di una localizzazione non UTF-8 obsoleta da lungo tempo non è più "
"supportato dai desktop e dagli altri progetti software più noti. Tali "
"localizzazioni dovrebbero essere aggiornate usando <command>dpkg-reconfigure "
"locales</command> e selezionando un valore predefinito UTF-8. Ci si dovrebbe "
"anche assicurare che gli utenti non scavalchino il valore predefinito per "
"usare una localizzazione obsoleta nel proprio ambiente."

#~ msgid ""
#~ "Lines in sources.list starting with <quote>deb ftp:</quote> and pointing "
#~ "to debian.org addresses should be changed into <quote>deb http:</quote> "
#~ "lines."
#~ msgstr ""
#~ "Le righe in sources.list che iniziano con <quote>deb ftp:</quote> e che "
#~ "puntano a indirizzi debian.org devono essere modificate in <quote>deb "
#~ "http:</quote>."

#~ msgid ""
#~ "<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
#~ "\">Debian will remove FTP access to all of its official mirrors on "
#~ "2017-11-01</ulink>.  If your sources.list contains a <literal>debian.org</"
#~ "literal> host, please consider switching to <ulink url=\"https://deb."
#~ "debian.org\">deb.debian.org</ulink>.  This note only applies to mirrors "
#~ "hosted by Debian itself.  If you use a secondary mirror or a third-party "
#~ "repository, then they may still support FTP access after that date.  "
#~ "Please consult with the operators of these if you are in doubt."
#~ msgstr ""
#~ "<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
#~ "\">Debian rimuoverà l'accesso FTP a tutti i suoi mirror ufficiali il "
#~ "giorno 2017-11-01</ulink>. Se il proprio file sources.list contiene un "
#~ "host <literal>debian.org</literal> considerare il passaggio a <ulink url="
#~ "\"https://deb.debian.org\">deb.debian.org</ulink>. Questo problema si "
#~ "applica solo ai mirror ospitati da Debian stessa. Se si usa un mirror "
#~ "secondario o un repository di terze parti allora questi potrebbero "
#~ "supportare ancora l'accesso FTP dopo tale data. In caso di dubbio, "
#~ "consultare gli amministratori di tali mirror."

#~ msgid ""
#~ "In the GNOME screensaver, using passwords with non-ASCII characters, "
#~ "pam_ldap support, or even the ability to unlock the screen may be "
#~ "unreliable when not using UTF-8.  The GNOME screenreader is affected by "
#~ "bug <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>.  The "
#~ "Nautilus file manager (and all glib-based programs, and likely all Qt-"
#~ "based programs too) assume that filenames are in UTF-8, while the shell "
#~ "assumes they are in the current locale's encoding. In daily use, non-"
#~ "ASCII filenames are just unusable in such setups.  Furthermore, the gnome-"
#~ "orca screen reader (which grants sight-impaired users access to the GNOME "
#~ "desktop environment) requires a UTF-8 locale since Squeeze; under a "
#~ "legacy characterset, it will be unable to read out window information for "
#~ "desktop elements such as Nautilus/GNOME Panel or the Alt-F1 menu."
#~ msgstr ""
#~ "Nel salvaschermo di GNOME, usare password con caratteri non ASCII, il "
#~ "supporto per pam_ldap o persino la capacità di sbloccare lo schermo "
#~ "possono essere non affidabili se non si usa UTF-8. Il salvaschermo di "
#~ "GNOME è affetto dal bug <ulink url=\"http://bugs.debian."
#~ "org/599197\">#599197</ulink>. Il gestore di file Nautilus (e tutti i "
#~ "programmi basati su glib e probabilmente anche tutti i programmi basati "
#~ "su Qt) presumono che i nomi di file siano in UTF-8, mentre la shell "
#~ "presume che siano nella codifica locale attuale. In questa situazione, "
#~ "nell'uso quotidiano, i nomi dei file non ASCII sono semplicemente non "
#~ "utilizzabili. Inoltre il lettore dello schermo gnome-orca (che permette "
#~ "l'accesso all'ambiente desktop GNOME agli utenti con difetti della vista) "
#~ "richiede una localizzazione UTF-8 a partire da Squeeze; con un insieme di "
#~ "caratteri obsoleto, non sarà in grado di leggere le informazioni sulle "
#~ "finestre per gli elementi del desktop come il pannello di GNOME o "
#~ "Nautilus, oppure il menu Alt-F1."

#~ msgid ""
#~ "If your system is localized and is using a locale that is not based on "
#~ "UTF-8 you should strongly consider switching your system over to using "
#~ "UTF-8 locales.  In the past, there have been bugs<placeholder type="
#~ "\"footnote\" id=\"0\"/> identified that manifest themselves only when "
#~ "using a non-UTF-8 locale. On the desktop, such legacy locales are "
#~ "supported through ugly hacks in the library internals, and we cannot "
#~ "decently provide support for users who still use them."
#~ msgstr ""
#~ "Se il proprio sistema è localizzato e usa un locale non basato su UTF-8 "
#~ "si dovrebbe considerare l'opportunità di passare a un locale UTF-8. In "
#~ "passato si sono verificati dei bug<placeholder type=\"footnote\" id=\"0\"/"
#~ "> che si manifestavano solo quando era in uso un locale non-UTF-8. I "
#~ "locale obsoleti sono supportati tramite dei bruttissimi trucchetti "
#~ "all'interno delle librerie e non è possibile fornire un supporto decente "
#~ "agli utenti che continuano a usarli."

#~ msgid ""
#~ "To configure your system's locale you can run <command>dpkg-reconfigure "
#~ "locales</command>. Ensure you select a UTF-8 locale when you are "
#~ "presented with the question asking which locale to use as a default in "
#~ "the system.  In addition, you should review the locale settings of your "
#~ "users and ensure that they do not have legacy locale definitions in their "
#~ "configuration environment."
#~ msgstr ""
#~ "È possibile usare <command>dpkg-reconfigure locales</command> per "
#~ "configurare il locale del proprio sistema. Quando viene chiesto quale "
#~ "locale usare come locale predefinito scegliere un locale UTF-8. Inoltre, "
#~ "è opportuno verificare le impostazioni di locale dei propri utenti per "
#~ "assicurarsi che nessuno utilizzi dei locale obsoleti nella configurazione "
#~ "d'ambiente."

#~ msgid ""
#~ "Since release 2:1.7.7-12, xorg-server no longer reads the file "
#~ "XF86Config-4.  See also <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
#~ msgstr ""
#~ "A partire dal rilascio 2:1.7.7-12, xorg-server non legge più il file "
#~ "XF86Config-4. Vedere anche <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
